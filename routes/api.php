<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', 'UserController@show')->middleware('auth:api');
Route::post('/user', 'UserController@register');
Route::post('/user/token', 'UserController@token');

Route::get('/todos', 'TodoItemController@list')->middleware('auth:api');
Route::get('/todos/{todoItem}', 'TodoItemController@show')
    ->middleware('auth:api');
Route::post('/todos', 'TodoItemController@store')->middleware('auth:api');
Route::put('/todos/{todoItem}', 'TodoItemController@edit')
    ->middleware('auth:api');
Route::delete('/todos/{todoItem}', 'TodoItemController@delete')
    ->middleware('auth:api');
