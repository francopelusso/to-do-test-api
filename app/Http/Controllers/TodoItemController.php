<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\TodoItem;

class TodoItemController extends Controller
{
    public function list()
    {
        $user = auth('api')->user();

        return TodoItem::where('user_id', '=', $user->id)->paginate(10);
    }

    public function show(TodoItem $todoItem)
    {
        $user = auth('api')->user();

        if ($user->id != $todoItem->user_id) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        return $todoItem;
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $todoData = $request->all();
        $todoData['user_id'] = 2;

        $todoItem = TodoItem::create($todoData);

        return $todoItem;
    }

    public function edit(Request $request, TodoItem $todoItem)
    {
        if (auth('api')->user()->id != $todoItem->user_id) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        $todoItem->update($request->all());
        return $todoItem;
    }

    public function delete(TodoItem $todoItem)
    {
        if (auth('api')->user()->id != $todoItem->user_id) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        $todoItem->delete();
        return response()->json(['success' => true], 204);
    }
}
