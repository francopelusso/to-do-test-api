<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\User;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $userData = $request->all();
        $userData['api_token'] = Str::random(60);
        $userData['password'] = bcrypt('password');
        User::create($userData);

        return $user;
    }

    public function token(Request $request)
    {
        $userData = $request->all();

        $validator = Validator::make($userData, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', '=', $userData['email'])->first();
        if (!$user) {
            return response()->json(['errors' => 'Unauthorized'], 401);
        }

        if (!Hash::check($userData['password'], $user->password)) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        return response()->json(['api_token' => $user->api_token]);
    }

    public function show()
    {
        return auth('api')->user();
    }
}
