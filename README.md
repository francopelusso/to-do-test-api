# To Do Test API

## Setup
1. Download repository: `git clone git@gitlab.com:/francopelusso/to-do-test-api.git`.
1. Change working directory: `cd to-do-test-api`.
1. Install [Laravel](https://laravel.com/).
1. Create database: `touch database/database.sqlite`.
1. Copy **.env.example** content to **.env**.
1. Change database configuration in **.env** file. Specifically **DB_DATABASE** to match full path to **./database/database.sqlite**.
1. Run migrations: `php artisan migrate --step`.
1. Seed database with dummy data: `php artisan db:seed`.
1. Start server: `php artisan serve`.

## Routes
**All routes begin with /api/**.
1. **/api/user/**:
    1. **GET**: If logged in, returns user data.
    1. **POST**: Register.
1. **/api/user/token**: **POST** ONLY. Provide user's email and password. Returns user's api_token if provided data is correct.
1. **/api/todos**:
    1. **GET**: List user's todo items.
    1. **POST**: Create todo item.
1. **/api/todos/{id}**:
    1. **GET**: Show user's single todo item data.
    1. **PUT**: Edit todo item data.
    1. **DELETE**: Delete todo item.
