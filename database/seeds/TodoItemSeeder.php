<?php

use Illuminate\Database\Seeder;
use App\TodoItem;
use App\User;

class TodoItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TodoItem::truncate();

        $userIds = User::pluck('id')->toArray();
        $faker = \Faker\Factory::create();
        $bools = [true, false];

        for ($i = 0; $i < 50; $i++) {
            TodoItem::create([
                'title' => $faker->sentence,
                'done' => $bools[array_rand($bools)],
                'user_id' => $userIds[array_rand($userIds)]
            ]);
        }
    }
}
