<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;
use App\TodoItem;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TodoItem::truncate();
        User::truncate();

        $faker = \Faker\Factory::create();
        $password = Hash::make('test-pass');

        $adminToken = Str::random(60);
        User::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => $password,
            'api_token' => $adminToken
        ]);

        for ($i = 0; $i < 5; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
                'api_token' => Str::random(60)
            ]);
        }

        echo "User API Token: $adminToken\n";
    }
}
